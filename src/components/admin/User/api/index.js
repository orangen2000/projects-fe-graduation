import request from "@/api/request";

export function detailDataUser(id) {
  return request({
    url: "user/detail?id=" + id,
    method: "get",
  });
}

export function listDataUser(payload) {
  return request({
    url: "user/list?page=" + payload.pageNumber + "&per_page=" + payload.pageSize,
    method: "get",
  });
}

export function createDataUser(payload) {
  return request({
    url: "user/create",
    method: "post",
    data: payload,
  });
}

export function editDataUser(payload, id) {
  return request({
    url: "user/update/" + id,
    method: "post",
    data: payload,
  });
}

export function deleteDataUser(id) {
  return request({
    url: "user/delete/" + id,
    method: "delete",
  });
}
