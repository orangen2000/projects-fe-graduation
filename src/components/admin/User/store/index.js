import {
  detailDataUser,
  listDataUser,
  createDataUser,
  editDataUser,
  deleteDataUser,
} from "@/components/admin/User/api/index";
import { CONSTANTS } from "@/utils/constant";

const SUCCESS = "SUCCESS";
const DETAIL_DATA_USER = "DETAIL_DATA_USER";
const LIST_DATA_USER = "LIST_DATA_USER";
const CREATE_DATA_USER = "CREATE_DATA_USER";
const EDIT_DATA_USER = "EDIT_DATA_USER";

export const state = {
  success: true,
  detailDataUser: null,
  listDataUser: null,
  createDataUser: null,
  editDataUser: null,
};

const mutations = {
  [SUCCESS](state, data) {
    state.success = data;
  },
  [DETAIL_DATA_USER](state, data) {
    state.detailDataUser = data;
  },
  [LIST_DATA_USER](state, data) {
    state.listDataUser = data;
  },
  [CREATE_DATA_USER](state, data) {
    state.createDataUser = data;
  },
  [EDIT_DATA_USER](state, data) {
    state.editDataUser = data;
  },
};

const getters = {};

const actions = {
  async detailUser({ commit }, { vue, id }) {
    let detailUsers = await detailDataUser(id);
    if (detailUsers.status === CONSTANTS.SUCCESS) {
      return commit(DETAIL_DATA_USER, detailUsers.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async listUser({ commit }, { vue, payload }) {
    let dataUsers = await listDataUser(payload);
    if (dataUsers.status === CONSTANTS.SUCCESS) {
      return commit(LIST_DATA_USER, dataUsers.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async createUser({ commit }, { vue, payload }) {
    let createUsers = await createDataUser(payload);
    if (createUsers.status === CONSTANTS.SUCCESS) {
      return commit(CREATE_DATA_USER, createUsers.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async editUser({ commit }, { vue, payload, id }) {
    let editUsers = await editDataUser(payload, id);
    if (editUsers.status === CONSTANTS.SUCCESS) {
      return commit(EDIT_DATA_USER, editUsers.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async deleteUser({ commit }, { vue, id }) {
    let deleteUser = await deleteDataUser(id);
    if (deleteUser.status === CONSTANTS.SUCCESS) {
      return commit(EDIT_DATA_USER, deleteUser.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
