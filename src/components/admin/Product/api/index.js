import request from "@/api/request";

export function detailDataProduct(id) {
  return request({
    url: "product/detail?id=" + id,
    method: "get",
  });
}

export function listDataProduct(payload) {
  return request({
    url: "product/list?page=" + payload.pageNumber + "&per_page=" + payload.pageSize,
    method: "get",
  });
}

export function createDataProduct(payload) {
  return request({
    url: "product/create",
    method: "post",
    data: payload,
  });
}

export function editDataProduct(payload, id) {
  return request({
    url: "product/update/" + id,
    method: "post",
    data: payload,
  });
}

export function deleteDataProduct(id) {
  return request({
    url: "product/delete/" + id,
    method: "delete",
  });
}
