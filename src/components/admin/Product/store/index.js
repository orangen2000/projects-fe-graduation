import {
  detailDataProduct,
  listDataProduct,
  createDataProduct,
  editDataProduct,
  deleteDataProduct,
} from "@/components/admin/Product/api/index";
import { CONSTANTS } from "@/utils/constant";

const SUCCESS = "SUCCESS";
const DETAIL_DATA_PRODUCT = "DETAIL_DATA_PRODUCT";
const LIST_DATA_PRODUCT = "LIST_DATA_PRODUCT";
const CREATE_DATA_PRODUCT = "CREATE_DATA_PRODUCT";
const EDIT_DATA_PRODUCT = "EDIT_DATA_PRODUCT";

export const state = {
  success: true,
  detailDataProduct: null,
  listDataProduct: null,
  createDataProduct: null,
  editDataProduct: null,
};

const mutations = {
  [SUCCESS](state, data) {
    state.success = data;
  },
  [DETAIL_DATA_PRODUCT](state, data) {
    state.detailDataProduct = data;
  },
  [LIST_DATA_PRODUCT](state, data) {
    state.listDataProduct = data;
  },
  [CREATE_DATA_PRODUCT](state, data) {
    state.createDataProduct = data;
  },
  [EDIT_DATA_PRODUCT](state, data) {
    state.editDataProduct = data;
  },
};

const getters = {};

const actions = {
  async detailProduct({ commit }, { vue, id }) {
    let detailProduct = await detailDataProduct(id);
    if (detailProduct.status === CONSTANTS.SUCCESS) {
      return commit(DETAIL_DATA_PRODUCT, detailProduct.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async listProduct({ commit }, { vue, payload }) {
    let listProduct = await listDataProduct(payload);
    if (listProduct.status === CONSTANTS.SUCCESS) {
      return commit(LIST_DATA_PRODUCT, listProduct.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async createProduct({ commit }, { vue, payload }) {
    let createProduct = await createDataProduct(payload);
    if (createProduct.status === CONSTANTS.SUCCESS) {
      return commit(CREATE_DATA_PRODUCT, createProduct.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async editProduct({ commit }, { vue, payload, id }) {
    let editProduct = await editDataProduct(payload, id);
    if (editProduct.status === CONSTANTS.SUCCESS) {
      return commit(EDIT_DATA_PRODUCT, editProduct.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async deleteProduct({ commit }, { id }) {
    let deleteProduct = await deleteDataProduct(id);
    if (deleteProduct.status === CONSTANTS.SUCCESS) {
      return commit(EDIT_DATA_PRODUCT, deleteProduct.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
