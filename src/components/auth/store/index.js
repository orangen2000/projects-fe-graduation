import {
  loginAccount,
  logoutAccount,
  changePassWordAccount,
  registerAccount,
} from "@/components/auth/api/index";
import { CONSTANTS } from "@/utils/constant";

const SUCCESS = "SUCCESS";
const LOGIN_ACCOUNT = "LOGIN_ACCOUNT";
const LOGOUT_ACCOUNT = "LOGOUT_ACCOUNT";
const CHANGE_PASSWORD_ACCOUNT = "CHANGE_PASSWORD_ACCOUNT";
const REGISTER_ACCOUNT = "REGISTER_ACCOUNT";

export const state = {
  success: true,
  loginData: null,
  logoutData: null,
  changePasswordData: null,
  registerData: null,
};

const mutations = {
  [SUCCESS](state, data) {
    state.success = data;
  },
  [LOGIN_ACCOUNT](state, data) {
    state.loginData = data;
  },
  [LOGOUT_ACCOUNT](state, data) {
    state.logoutData = data;
  },
  [CHANGE_PASSWORD_ACCOUNT](state, data) {
    state.changePasswordData = data;
  },
  [REGISTER_ACCOUNT](state, data) {
    state.registerData = data;
  },
};

const getters = {};

const actions = {
  async loginAccount({ commit }, { vue, payload }) {
    let dataLogin = await loginAccount(payload);
    if (dataLogin.status === CONSTANTS.SUCCESS) {
      return commit(LOGIN_ACCOUNT, dataLogin.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async logoutAccount({ commit }, { vue }) {
    let dataLogout = await logoutAccount();
    if (dataLogout.status === CONSTANTS.SUCCESS) {
      return commit(LOGOUT_ACCOUNT, dataLogout.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async changePasswordAccount({ commit }, { vue, payload }) {
    let dataChangePassword = await changePassWordAccount(payload);
    if (dataChangePassword.status === CONSTANTS.SUCCESS) {
      return commit(CHANGE_PASSWORD_ACCOUNT, dataChangePassword.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },

  async registerAccount({ commit }, { vue, payload }) {
    let dataRegister = await registerAccount(payload);
    if (dataRegister.status === CONSTANTS.SUCCESS) {
      return commit(REGISTER_ACCOUNT, dataRegister.data);
    }

    return vue.$message.error("Lỗi mới này chưa được định nghĩa");
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
