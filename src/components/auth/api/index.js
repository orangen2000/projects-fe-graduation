import request from "@/api/request";

export function loginAccount(payload) {
  return request({
    url: "login",
    method: "post",
    data: payload,
  });
}

export function logoutAccount() {
  return request({
    url: "logout",
    method: "post",
  });
}

export function changePassWordAccount(payload) {
  return request({
    url: "change-password",
    method: "post",
    data: payload,
  });
}

export function registerAccount(payload) {
  return request({
    url: "register",
    method: "post",
    data: payload,
  });
}
