export const CONSTANTS = {
  SUCCESS: 200,
};
export const LOADING = "[mutations] show loading";
export const FORMAT = {
  SPECIAL: /^[a-zA-Z0-9\_.@]+$/g,
  EMAIL: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
};
