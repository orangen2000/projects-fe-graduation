import { getToken, removeToken } from "@/api/auth";
import axios from "axios";

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 600000,
});

service.interceptors.request.use(
  (config) => {
    const token = getToken();
    if (token) {
      config.headers["Authorization"] = "Bearer " + `${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  (response) => {
    return response.data;
  },
  async (err) => {
    if (
      err.response.status === 401 &&
      err.response.statusText === "Unauthorized"
    ) {
      window.localStorage.removeItem("x-Project-token");
      window.localStorage.removeItem("authLogin");
      window.location.reload();
    } else {
      return Promise.reject(err);
    }
  }
);

export default service;
