import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/login",
    name: "Login",
    component: () => import("@/components/auth/views/Login"),
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("@/components/auth/views/Register"),
  },
  {
    path: "/",
    component: () => import("@/components/page/Layout"),
    children: [
      {
        path: "home",
        name: "Home",
        component: () => import("@/components/page/Home"),
      },
    ],
  },
  {
    path: "/",
    component: () => import("@/components/admin/LayoutAdmin"),
    meta: {
      requiresAuth: true,
      accessedBy: ["admin"],
    },
    children: [
      {
        path: "home-admin",
        name: "HomeAdmin",
        component: () => import("@/components/admin/HomeAdmin"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "profile",
        name: "Profile",
        component: () => import("@/components/auth/views/Profile"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "change-password",
        name: "ChangePassword",
        component: () => import("@/components/auth/views/ChangePassword"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "list-product",
        name: "ListProduct",
        component: () => import("@/components/admin/Product/views/ListProduct"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "create-product",
        name: "CreateProduct",
        component: () => import("@/components/admin/Product/views/CreateOrEditProduct"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "edit-product/:id",
        name: "EditProduct",
        component: () => import("@/components/admin/Product/views/CreateOrEditProduct"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "list-user",
        name: "ListUser",
        component: () => import("@/components/admin/User/views/ListUser"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "create-user",
        name: "CreateUser",
        component: () => import("@/components/admin/User/views/CreateOrEditUser"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
      {
        path: "edit-user/:id",
        name: "EditUser",
        component: () => import("@/components/admin/User/views/CreateOrEditUser"),
        meta: {
          requiresAuth: true,
          accessedBy: ["admin"],
        },
      },
    ],
  },
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    component: () => import("@/components/status/404"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
