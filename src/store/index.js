import { createStore } from "vuex";
import { LOADING } from "@/utils/constant";
import storeLayout from "@/components/layout/store/index";
import storeAuth from "@/components/auth/store/index";
import storeProduct from "@/components/admin/Product/store/index";
import storeUser from "@/components/admin/User/store/index";

export default createStore({
  modules: {
    storeLayout,
    storeAuth,
    storeProduct,
    storeUser,
  },
  mutations: {
    [LOADING](state, payload) {
      state.showLoading = payload;
    },
  },
  state: {
    unsubscribes: [],
  },
});
